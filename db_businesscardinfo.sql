-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 18, 2019 at 04:29 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_businesscardinfo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_info`
--

CREATE TABLE `tbl_info` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `designation` text,
  `contact_no` varchar(13) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_info`
--

INSERT INTO `tbl_info` (`id`, `name`, `address`, `company`, `designation`, `contact_no`, `email`) VALUES
(1, 'Weeks, Elton G.', 'P.O. Box 784, 9958 Vulputate Rd.', 'Dictum Augue Malesuada Institute', 'Fusce feugiat.', '16450124-1758', 'libero@egetvolutpat.co.uk'),
(2, 'Hart, Hadassah T.', 'Ap #553-4021 Nec St.', 'Aliquet Diam Sed Incorporated', 'euismod enim.', '16431221-2998', 'fermentum.arcu.Vestibulum@luctuset.net'),
(3, 'Cooke, Rahim T.', 'P.O. Box 226, 5737 Cubilia St.', 'Mauris Morbi Non Consulting', 'sed dolor.', '16861129-3963', 'justo@dapibus.net'),
(4, 'Hurst, Lara B.', '824-2214 Lorem Road', 'Ridiculus Mus Proin Corporation', 'Integer id', '16830222-1067', 'odio.Phasellus@Maurismagna.org'),
(5, 'Frye, Phyllis H.', 'P.O. Box 570, 2816 Nulla St.', 'Dis Parturient Limited', 'consequat auctor,', '16800717-7655', 'et.malesuada.fames@Phasellus.net'),
(6, 'Bender, Eugenia I.', 'Ap #779-3404 Vel Street', 'Urna Ltd', 'magna. Cras', '16081107-4467', 'elementum@uteratSed.co.uk'),
(7, 'Baxter, Evan D.', 'P.O. Box 345, 1223 Sollicitudin Road', 'Libero Proin LLC', 'In tincidunt', '16540812-0177', 'fringilla@velitegetlaoreet.net'),
(8, 'Bean, Rashad C.', 'Ap #823-3519 Arcu Street', 'Sit Amet Consulting', 'Nulla interdum.', '16260207-2874', 'sodales@Class.edu'),
(9, 'Green, Leila F.', '1637 Mi Avenue', 'Sed Ltd', 'enim nisl', '16511030-8854', 'Curabitur.ut@euelit.net'),
(10, 'Obrien, Megan T.', '9820 Nibh. Ave', 'Id Sapien Cras PC', 'fringilla. Donec', '16110423-4826', 'at@necurna.org'),
(11, 'Hunter, Wayne Z.', '8981 Placerat, St.', 'Dolor Sit Amet Ltd', 'dapibus rutrum,', '16450502-0398', 'Cras.vulputate.velit@sollicitudincommodoipsum.com'),
(12, 'Meyers, Shad M.', 'P.O. Box 481, 2906 Mollis St.', 'Quisque Corp.', 'leo. Vivamus', '16070707-2112', 'gravida.non@cursusa.net'),
(13, 'Lambert, Maisie W.', 'P.O. Box 642, 2939 At Rd.', 'Felis Institute', 'et arcu', '16150211-0792', 'montes.nascetur.ridiculus@odiosagittissemper.net'),
(14, 'Day, Belle S.', '299-8771 Phasellus Ave', 'Pharetra Corporation', 'odio semper', '16310208-3056', 'feugiat.tellus@orcilobortis.net'),
(15, 'Byers, Lucian G.', 'P.O. Box 392, 8149 Leo Road', 'Non Enim Mauris Institute', 'risus. Duis', '16461202-8631', 'arcu.iaculis@tellusidnunc.net'),
(16, 'Adkins, Garth P.', 'P.O. Box 730, 763 Ultricies Avenue', 'Nullam Feugiat Corporation', 'leo. Morbi', '16690621-0809', 'Mauris.blandit.enim@arcuiaculis.edu'),
(17, 'Cantrell, Emma A.', 'Ap #364-8019 Arcu Ave', 'Sapien Incorporated', 'consequat purus.', '16420427-6796', 'Maecenas.malesuada.fringilla@laoreetposuere.com'),
(18, 'Thomas, Kirestin C.', '7686 Vestibulum Rd.', 'Non Vestibulum Company', 'vitae, erat.', '16350212-6125', 'nulla.Integer@mi.edu'),
(19, 'Roberson, Zia S.', '8431 Tempus Road', 'Egestas A Inc.', 'metus urna', '16540718-4752', 'sagittis@nullaCraseu.co.uk'),
(20, 'Randolph, Phelan U.', 'Ap #460-1976 Integer Avenue', 'Mauris Aliquam Industries', 'Nam tempor', '16750906-4106', 'odio@libero.co.uk'),
(21, 'Morales, Amber A.', '4103 Phasellus St.', 'Duis Corporation', 'cursus non,', '16781109-7737', 'erat.Vivamus@mauriserat.com'),
(22, 'Browning, Carla S.', 'P.O. Box 357, 5580 Ante Rd.', 'Mauris Limited', 'pellentesque. Sed', '16740403-7843', 'amet.diam.eu@Phasellus.com'),
(23, 'Lawrence, Tara C.', 'Ap #134-1347 Lorem. Road', 'Dignissim Lacus Foundation', 'Quisque libero', '16980710-7314', 'Morbi@ullamcorpermagnaSed.edu'),
(24, 'Goff, Samuel T.', 'Ap #334-2579 Ipsum St.', 'Varius Ultrices LLP', 'neque non', '16080128-0686', 'eu@inmolestietortor.org'),
(25, 'Eaton, Solomon M.', 'P.O. Box 268, 6379 Sed St.', 'Commodo Corp.', 'magna a', '16351102-5292', 'laoreet@volutpatNulladignissim.org'),
(26, 'Cameron, Juliet Q.', '7930 Eu St.', 'Massa Vestibulum Accumsan LLC', 'Aliquam gravida', '16270621-0057', 'ridiculus.mus@aptenttaciti.org'),
(27, 'Hull, Hayfa J.', 'P.O. Box 843, 6504 Convallis Rd.', 'Magnis Dis Parturient Consulting', 'Mauris nulla.', '16710926-4510', 'Nullam.enim.Sed@atpretiumaliquet.co.uk'),
(28, 'Scott, Isabella O.', 'Ap #303-5451 Nec Avenue', 'Sodales Mauris Blandit Foundation', 'fames ac', '16881226-2809', 'orci.luctus.et@insodaleselit.ca'),
(29, 'Melton, Lewis C.', 'Ap #320-8820 Augue, Ave', 'Morbi Non Company', 'lorem vitae', '16981006-4304', 'ac@a.edu'),
(30, 'Huber, Ora Z.', '118-1540 Cras Rd.', 'Tortor Nunc Commodo Incorporated', 'quam dignissim', '16700815-2022', 'tempor.est@Vestibulumanteipsum.net'),
(31, 'Estrada, Hedda D.', 'Ap #874-9739 Ornare. Road', 'Nec Foundation', 'at pede.', '16400106-4700', 'amet@et.com'),
(32, 'Dorsey, Todd E.', '704-3723 Cursus Rd.', 'Orci Corporation', 'accumsan convallis,', '16740820-5313', 'enim.nec.tempus@ametrisus.net'),
(33, 'Greer, Roanna W.', '954-166 Diam. St.', 'Aliquam Inc.', 'massa. Suspendisse', '16520310-2057', 'gravida.sit@porttitortellusnon.co.uk'),
(34, 'Stewart, Chelsea Z.', '6155 Rhoncus. Rd.', 'Nulla Aliquet Institute', 'bibendum fermentum', '16790630-5672', 'augue@necurnaet.edu'),
(35, 'Benjamin, Hanae M.', 'Ap #287-1512 Ut Rd.', 'Urna Company', 'est ac', '16200315-1335', 'at@justoeu.co.uk'),
(36, 'Cunningham, Evelyn U.', 'Ap #670-568 Egestas Rd.', 'Maecenas Malesuada Inc.', 'tempus eu,', '16110511-4837', 'nibh.lacinia.orci@rutrum.edu'),
(37, 'Rich, Hedley X.', '914-8153 Ridiculus St.', 'Ridiculus Mus Incorporated', 'facilisis lorem', '16880230-8463', 'enim.nisl.elementum@ultriciesornareelit.org'),
(38, 'Sawyer, Russell M.', '5472 Egestas Street', 'Nam Nulla Consulting', 'ut, nulla.', '16920928-2517', 'a.arcu.Sed@Duiselementumdui.ca'),
(39, 'Bender, Jelani Q.', 'Ap #586-1169 Lorem Rd.', 'Montes Nascetur LLC', 'dolor dolor,', '16120201-4369', 'Curabitur.sed.tortor@Quisqueornaretortor.net'),
(40, 'Lindsay, Cedric A.', '5292 Turpis. Road', 'Massa Foundation', 'cubilia Curae;', '16730629-0706', 'cursus.purus@Maurisvestibulumneque.co.uk'),
(41, 'Jennings, Hedwig Y.', '617-7676 Amet, Road', 'Erat Volutpat Ltd', 'luctus ut,', '16711229-7192', 'Cum.sociis.natoque@indolor.net'),
(42, 'Orr, Flavia C.', '557-8089 Duis Avenue', 'Suscipit LLP', 'ut, sem.', '16320801-7495', 'magna.Nam.ligula@mollisInteger.net'),
(43, 'Mathews, Wynter C.', '5598 Orci Rd.', 'Et Industries', 'molestie pharetra', '16860319-7198', 'iaculis@posuerevulputatelacus.ca'),
(44, 'Deleon, Chaim Z.', 'P.O. Box 652, 5861 Massa St.', 'Duis Cursus Diam Company', 'enim, sit', '16390522-3099', 'euismod@auctor.com'),
(45, 'Whitney, Barclay E.', 'P.O. Box 829, 5723 Adipiscing Rd.', 'Tristique Inc.', 'ut mi.', '16560111-7533', 'diam.eu.dolor@utpellentesque.org'),
(46, 'Travis, Chadwick N.', 'P.O. Box 877, 3433 Pretium Rd.', 'Magna Praesent Associates', 'et, rutrum', '16910219-7010', 'rutrum.urna@sit.com'),
(47, 'Aguirre, Arden O.', '442-8593 Bibendum Rd.', 'Mi Industries', 'Integer eu', '16130310-6080', 'Sed@ac.org'),
(48, 'Weber, Carlos W.', '1803 A, Av.', 'A Dui Cras LLP', 'ultrices iaculis', '16550925-1491', 'lorem.vehicula@sedtortorInteger.com'),
(49, 'Holmes, Germaine A.', '499-4809 Aenean Avenue', 'In Ltd', 'urna, nec', '16630317-1679', 'nec.ante.Maecenas@nonummyultricies.co.uk'),
(50, 'Dunlap, Mollie J.', '1033 Enim, Ave', 'Ut Mi Duis Incorporated', 'per inceptos', '16890112-4449', 'nec@Duis.org'),
(51, 'Cunningham, Macey Z.', 'Ap #536-8411 Aliquam Av.', 'Nam LLP', 'inceptos hymenaeos.', '16660629-3303', 'eget.odio.Aliquam@nuncullamcorper.co.uk'),
(52, 'Fischer, Piper S.', '494-9188 Phasellus St.', 'In Sodales LLC', 'faucibus leo,', '16670914-1490', 'a.felis@accumsansed.com'),
(53, 'Mccray, Sandra E.', 'P.O. Box 883, 9297 Mauris St.', 'Orci LLC', 'sapien. Aenean', '16421024-9217', 'Sed@egestas.com'),
(54, 'Stephenson, Rebekah Q.', '775-6974 Et Street', 'Metus Vitae Velit Incorporated', 'Donec porttitor', '16981115-3650', 'tortor@consequatnecmollis.ca'),
(55, 'Goff, Karina Q.', 'P.O. Box 964, 7111 Sapien St.', 'Erat Incorporated', 'urna et', '16920618-7701', 'placerat.eget@ullamcorpereu.co.uk'),
(56, 'Oneill, Catherine N.', 'P.O. Box 210, 1085 Donec Rd.', 'Erat Eget Ipsum Consulting', 'pede. Cum', '16570729-6173', 'in@ipsumacmi.co.uk'),
(57, 'Harding, Wylie X.', 'Ap #616-992 Feugiat Av.', 'Ipsum Nunc Ltd', 'dictum mi,', '16630403-3589', 'netus.et.malesuada@libero.ca'),
(58, 'Peters, Ayanna Z.', '667 Metus. Av.', 'Phasellus Vitae Mauris Inc.', 'vitae erat', '16550729-6407', 'Sed@auctor.com'),
(59, 'Mays, Tanner Y.', '873-4569 Natoque Av.', 'Risus Ltd', 'egestas blandit.', '16270507-3332', 'Phasellus.in.felis@Nuncsollicitudin.org'),
(60, 'Barnett, Oprah Y.', 'Ap #566-2277 Fringilla Avenue', 'Mauris Quis Turpis Ltd', 'Donec tincidunt.', '16600106-9266', 'lectus@enim.edu'),
(61, 'Leach, Ethan A.', 'P.O. Box 747, 3237 Neque. Avenue', 'Massa Institute', 'ut lacus.', '16440928-5527', 'orci.Ut.sagittis@Phasellusvitaemauris.org'),
(62, 'Hartman, Anthony H.', '316-8524 Egestas Rd.', 'Et Magnis Corp.', 'semper rutrum.', '16151003-0909', 'laoreet.libero@auctor.edu'),
(63, 'Williamson, Natalie M.', 'P.O. Box 305, 1943 Volutpat. Road', 'Nulla Semper Tellus PC', 'mattis ornare,', '16620616-2106', 'orci.lobortis@auctor.ca'),
(64, 'Stanley, Hayley Z.', 'Ap #377-7555 Vel Ave', 'Ac Turpis Egestas Corporation', 'eget ipsum.', '16320902-7428', 'sapien.gravida@nuncullamcorpereu.net'),
(65, 'Sampson, Sybil G.', '1083 Rutrum Road', 'Sollicitudin Associates', 'Aenean euismod', '16730806-6948', 'Vestibulum@sem.co.uk'),
(66, 'Barber, Brenden C.', '7572 Euismod St.', 'Posuere Consulting', 'ac mattis', '16660628-2553', 'lectus.pede.ultrices@sedpedenec.edu'),
(67, 'Miller, Branden E.', '779-8208 Elementum Av.', 'Cras Dolor Dolor Limited', 'elit. Aliquam', '16550825-5014', 'orci.Ut@velvulputateeu.ca'),
(68, 'Mcpherson, Mufutau D.', 'P.O. Box 547, 275 Diam. Street', 'Feugiat Associates', 'Cum sociis', '16180422-1222', 'a.felis@euaccumsan.co.uk'),
(69, 'Todd, Ila V.', '967-2368 Ut, Ave', 'Nullam Velit Dui Corporation', 'lorem semper', '16910820-8266', 'Nunc.ac@rhoncusDonecest.net'),
(70, 'Warren, Amy L.', 'Ap #186-8795 Mattis Rd.', 'Ipsum Suspendisse Institute', 'neque. Morbi', '16221020-5494', 'Praesent.interdum@nibhdolornonummy.net'),
(71, 'Romero, Zachery H.', '827-6274 Risus. Street', 'Aliquet Metus Ltd', 'aliquet, sem', '16490120-6914', 'enim@semperpretiumneque.net'),
(72, 'Mccarty, Joan Q.', '250-4126 Volutpat. Street', 'Dolor Foundation', 'lorem tristique', '16100525-1564', 'sem.molestie@Maecenasiaculisaliquet.com'),
(73, 'Burton, Dane U.', '179-4467 Eu St.', 'Nam Porttitor Scelerisque Corp.', 'orci, consectetuer', '16161015-7842', 'libero@senectus.co.uk'),
(74, 'Battle, Camden L.', 'Ap #353-6169 Vel Street', 'Morbi Neque PC', 'Integer vitae', '16290418-7321', 'commodo.at@sedturpisnec.edu'),
(75, 'Hartman, Ina Y.', '356-1676 Magnis Av.', 'Nunc Sollicitudin Incorporated', 'metus sit', '16371122-3887', 'ultrices@malesuada.org'),
(76, 'Pratt, Hunter U.', 'Ap #799-7644 Risus. Av.', 'Aliquet Magna A Consulting', 'Cras sed', '16540326-5142', 'eu@felis.com'),
(77, 'Terrell, Ima S.', 'P.O. Box 427, 7846 Convallis, St.', 'Iaculis Limited', 'non, vestibulum', '16420813-6517', 'urna@risusQuisquelibero.org'),
(78, 'Bean, Summer R.', '231-3805 Non, Road', 'Sed Neque Industries', 'turpis vitae', '16980501-0999', 'ullamcorper@acrisus.edu'),
(79, 'Lewis, Jonas N.', 'Ap #975-2402 Rutrum Rd.', 'Lacus Aliquam Foundation', 'ligula. Aenean', '16231219-0172', 'natoque.penatibus.et@necligula.com'),
(80, 'Francis, Rogan I.', '3265 Faucibus. Street', 'Montes Nascetur LLP', 'Quisque porttitor', '16330113-9949', 'nibh.dolor@lacusvarius.com'),
(81, 'Clemons, Hanna R.', 'P.O. Box 444, 8107 Nisl. St.', 'In Consequat Corporation', 'eleifend vitae,', '16821009-8680', 'sed@sed.org'),
(82, 'Hampton, Hanae X.', 'P.O. Box 140, 4570 Ultrices St.', 'Cras Interdum Nunc Foundation', 'congue, elit', '16030725-5414', 'enim.Sed.nulla@ultricesposuerecubilia.net'),
(83, 'Stokes, Christine K.', 'Ap #881-1253 Aliquam Av.', 'Hendrerit Donec Associates', 'commodo tincidunt', '16400803-1108', 'magna.malesuada.vel@Ut.co.uk'),
(84, 'Poole, Orla M.', 'P.O. Box 612, 6561 Nunc Avenue', 'Sem Elit LLP', 'in sodales', '16451207-4966', 'lorem.eu.metus@laoreetposuere.ca'),
(85, 'Ayers, Armando I.', '5136 Nec, Street', 'Risus Inc.', 'Phasellus dapibus', '16670813-1898', 'montes@malesuadavel.net'),
(86, 'Walter, Chaney T.', 'P.O. Box 646, 5328 Semper Rd.', 'Nec Ltd', 'metus vitae', '16900910-1149', 'erat.in.consectetuer@utmi.ca'),
(87, 'Townsend, Kaseem S.', '791-345 Mauris Road', 'Cras Vulputate Velit Consulting', 'diam. Pellentesque', '16250205-1853', 'Sed.eget@nequeNullam.org'),
(88, 'Warren, Erin Q.', '284-2420 Donec Av.', 'Praesent Eu Dui Ltd', 'nulla vulputate', '16090926-8575', 'risus.In@euturpisNulla.com'),
(89, 'Spencer, Gemma G.', '772 Aliquam Rd.', 'Tristique Neque Associates', 'amet, dapibus', '16170605-4242', 'vulputate.velit@Nullam.net'),
(90, 'Joyner, Damian D.', 'P.O. Box 687, 7538 Tempor, Rd.', 'Proin Sed Corporation', 'dui nec', '16600127-8404', 'vitae.dolor.Donec@etnetus.net'),
(91, 'Sullivan, Maisie R.', '4274 Molestie Avenue', 'Mauris Morbi Non Corporation', 'vel pede', '16270817-8799', 'dapibus@liberoMorbiaccumsan.net'),
(92, 'Mitchell, Camden T.', '9505 Integer Rd.', 'Quis Accumsan Convallis Limited', 'Nullam vitae', '16960628-5014', 'et.magnis.dis@Fuscemollis.edu'),
(93, 'Day, Natalie D.', 'Ap #269-3152 Et, Rd.', 'Dolor Dolor Tempus Corp.', 'varius ultrices,', '16831129-8049', 'nisi@nequesedsem.co.uk'),
(94, 'Justice, Stephen R.', 'Ap #630-3673 Nam Rd.', 'Est Arcu Company', 'ultrices sit', '16530907-4895', 'dolor.quam@Aeneansed.com'),
(95, 'Patrick, Hop E.', 'P.O. Box 966, 3338 Magna Rd.', 'Tincidunt Neque Corp.', 'penatibus et', '16610919-8371', 'ut@accumsaninterdumlibero.co.uk'),
(96, 'Buckner, Mia H.', 'Ap #377-4362 Dolor Rd.', 'Sagittis Duis Corporation', 'est. Mauris', '16420116-6966', 'pretium@est.com'),
(97, 'Avery, Aidan I.', 'Ap #562-8390 Sed Av.', 'Et Magna Limited', 'non, egestas', '16091002-7036', 'blandit.Nam.nulla@euultrices.edu'),
(98, 'Blankenship, Bree W.', '663-5676 Cursus Av.', 'Magna Company', 'blandit viverra.', '16610313-3176', 'lobortis.nisi@ametrisusDonec.net'),
(99, 'Fry, Melvin C.', 'P.O. Box 817, 5452 Magna St.', 'Odio Limited', 'et nunc.', '16861004-2213', 'a.magna.Lorem@scelerisque.edu'),
(100, 'Walls, Tad N.', 'P.O. Box 653, 2675 Etiam Avenue', 'Arcu Vel Quam PC', 'Aliquam ultrices', '16101027-3678', 'Nulla@idrisusquis.org'),
(101, 'Marson Vida', 'Naga City', 'UNC', 'Programmer', '914387185', 'johnmarson.vida@unc.edu.ph');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_info`
--
ALTER TABLE `tbl_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_info`
--
ALTER TABLE `tbl_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
