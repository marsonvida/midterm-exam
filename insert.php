<?php  
 $connect = mysqli_connect("localhost", "root", "", "db_businesscardinfo");  
 // define how many results you want per page
$results_per_page = 10;

 if(!empty($_POST))  
 {  
      $output = '';  
      $message = '';  
      $name = mysqli_real_escape_string($connect, $_POST["name"]);  
      $address = mysqli_real_escape_string($connect, $_POST["address"]);  
      $company = mysqli_real_escape_string($connect, $_POST["company"]);  
      $designation = mysqli_real_escape_string($connect, $_POST["designation"]);  
      $contact_no = mysqli_real_escape_string($connect, $_POST["contact_no"]);  
      $email = mysqli_real_escape_string($connect, $_POST["email"]);  
      if($_POST["employee_id"] != '')  
      {  
           $query = "  
           UPDATE tbl_info   
           SET name='$name',   
           address='$address',   
           company='$company',   
           designation = '$designation',   
           contact_no = '$contact_no'   
           email = '$email'   
           WHERE id='".$_POST["employee_id"]."'";  
           $message = 'Data Updated';  
      }  
      else  
      {  
           $query = "  
           INSERT INTO tbl_info(name, address, company, designation, contact_no, email)  
           VALUES('$name', '$address', '$company', '$designation', '$contact_no', '$email');  
           ";  
           $message = 'Data Successfully Saved.';  
      }  
      if(mysqli_query($connect, $query))  
      {  
           $output .= '<label class="text-success">' . $message . '</label>';  
           $select_query = "SELECT * FROM tbl_info ORDER BY name";  
           $result = mysqli_query($connect, $select_query);  
           $number_of_results = mysqli_num_rows($result);

            // determine number of total pages available
            $number_of_pages = ceil($number_of_results/$results_per_page);

            // determine which page number visitor is currently on
            if (!isset($_GET['page'])) {
            $page = 1;
            } else {
            $page = $_GET['page'];
            }

            // determine the sql LIMIT starting number for the results on the displaying page
            $this_page_first_result = ($page-1)*$results_per_page;

           $output .= '  
                <table class="table table-bordered">  
                     <tr>  
                        <th width="55%">Employee Name</th>  
                        <th width="15%">View</th>  
                        <th width="15%">Edit</th>  
                        <th width="15%">Delete</th>  
                     </tr>  
           ';  

            // retrieve selected results from database and display them on page
            $query='SELECT * FROM tbl_info ORDER by name LIMIT ' . $this_page_first_result . ',' .  $results_per_page;
            $result = mysqli_query($connect, $query);
           while($row = mysqli_fetch_array($result))  
           {  
                $output .= '  
                     <tr>  
                          <td>' . $row["name"] . '</td>  
                          <td><input type="button" name="view" value="view" id="' . $row["id"] . '" class="btn btn-info btn-xs view_data" /></td>  
                          <td><input type="button" name="edit" value="Edit" id="'.$row["id"] .'" class="btn btn-success btn-xs edit_data" /></td>  
                          <td><input type="button" name="delete" value="Delete" id="'.$row["id"] .'" class="btn btn-danger btn-xs delete_data" /></td>  
                          
                     </tr>  
                ';  
            }  
            $output .= '</table>';  
        }  
        echo $output; 
        echo '<div align="right"> <h4>';
                for ($page=1;$page<=$number_of_pages;$page++) {
                    echo '<a href="index.php?page=' . $page . '">' . $page . '</a> ';
                }
        echo '</h4> </div>';
 }  
 ?>